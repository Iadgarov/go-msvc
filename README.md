# Hello World 
[![pipeline status](https://gitlab.com/Iadgarov/go-msvc/badges/master/pipeline.svg)](https://gitlab.com/Iadgarov/go-msvc/commits/master) 
[![coverage report](https://gitlab.com/Iadgarov/go-msvc/badges/master/coverage.svg)](https://gitlab.com/Iadgarov/go-msvc/commits/master) 
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/iadgarov/go-msvc)](https://goreportcard.com/report/gitlab.com/iadgarov/go-msvc) 
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

![Banner](assets/banner.png)

A 'Hello World' golang microservice.

Written with Hexagonal architecture. 




