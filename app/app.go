package app

import (
	"context"
	"log"
	"os"
	"os/signal"

	"github.com/pkg/errors"
)

// Driver defines a driving adapter httpDriver
type Driver interface {
	Start() error
	Stop(ctx context.Context) error
}

// App defines the app we'll be running
type App struct {
	httpDriver Driver
}

// NewApp returns a new instance of App
func NewApp(s Driver) *App {
	return &App{
		httpDriver: s,
	}
}

// Go starts the app
func (a *App) Go() error {

	cSig := make(chan os.Signal, 1)
	signal.Notify(cSig, os.Interrupt)

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		c := <-cSig
		log.Printf("Received system call: %+v", c)
		cancel()
	}()

	return a.deployDrivers(ctx)
}

// Stop triggers a graceful shutdown of the app
func (a *App) Stop(ctx context.Context) error {
	if err := a.httpDriver.Stop(ctx); err != nil {
		return errors.Wrap(err, "Failed to stop HTTP server")
	}
	return nil
}

func (a *App) deployDrivers(ctx context.Context) error {
	driverStartErrChan := make(chan error, 1)
	go func() {
		if err := a.httpDriver.Start(); err != nil {
			driverStartErrChan <- err
		}
	}()

	select {
	case <-ctx.Done():
		return nil
	case err := <-driverStartErrChan:
		return errors.Wrap(err, "Failed to start drivers")
	}
}
