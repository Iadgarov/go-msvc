package http

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

const (
	urlParamLanguage = "lang"
	languageEnglish  = "english"
)

// HelloWorld handles HTTP GET requests to get a 'hello world' message in the chosen language
func (d *Driver) HelloWorld(w http.ResponseWriter, r *http.Request) {
	lang := chi.URLParam(r, urlParamLanguage)
	d.helloWorldByLanguageHandler(w, r, lang)
}

// HelloWorldEnglish handles HTTP GET requests to get the English version of the 'hello world' message
func (d *Driver) HelloWorldEnglish(w http.ResponseWriter, r *http.Request) {
	d.helloWorldByLanguageHandler(w, r, languageEnglish)
}

func (d *Driver) helloWorldByLanguageHandler(w http.ResponseWriter, r *http.Request, lang string) {
	log.Printf("[HTTP Driving Adapter] Handling %s %s request\n", r.Method, r.URL)
	msg, err := d.hw.HelloWorld(lang)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		if _, err := w.Write([]byte(err.Error())); err != nil {
			log.Println("Failed to write HTTP response")
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	if _, err := w.Write([]byte(msg)); err != nil {
		log.Println("Failed to write HTTP response")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
