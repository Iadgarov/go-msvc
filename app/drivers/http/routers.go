package http

import (
	"log"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func (d *Driver) routes() *chi.Mux {
	log.Println("Defining HTTP routes...")
	router := chi.NewRouter()

	router.Use(middleware.Timeout(d.timeout))
	router.Use(middleware.Logger)
	router.Route("/hello", func(r chi.Router) {
		r.Get("/", d.HelloWorldEnglish)
		r.Get("/{lang}", d.HelloWorld)
	})
	return router
}
