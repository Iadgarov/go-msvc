package http

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/pkg/errors"
)

const (
	timeout = 15
	port    = 8080
)

// Server is an interface for the golang http pkg. We define this to allow for mocking in tests
type Server interface {
	ListenAndServe() error
	Shutdown(ctx context.Context) error
}

// HelloWorld is an interface for the hello world domain
type HelloWorld interface {
	HelloWorld(lang string) (string, error)
}

// Driver defines an HTTP driving adapter
type Driver struct {
	server  Server
	timeout time.Duration

	hw HelloWorld
}

// Start boots up the HTTP server
func (d *Driver) Start() error {
	log.Println("Starting HTTP server...")
	return d.server.ListenAndServe()
}

// Stop handles graceful shutdown of the http driver
func (d *Driver) Stop(ctx context.Context) error {
	log.Println("Shutting down HTTP server...")
	if err := d.server.Shutdown(ctx); err != nil {
		return errors.Wrap(err, "failed to gracefully shutdown HTTP server")
	}
	return nil
}

// NewDriver defines a provider for the HTTP driver
func NewDriver(hw HelloWorld) *Driver {
	log.Println("Creating new HTTP driver...")
	d := Driver{
		hw:      hw,
		timeout: timeout * time.Second,
	}
	d.server = &http.Server{
		Handler: d.routes(),
		Addr:    ":" + strconv.Itoa(port),
	}
	return &d
}
