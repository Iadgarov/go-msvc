package helloworld

import (
	"log"

	"github.com/pkg/errors"
)

// Repository exposes 'get by language' operations for the 'hello world' message
type Repository interface {
	GetHelloWorldByLanguage(lang string) (string, error)
}

// Service defines the hello world service
type Service struct {
	repo Repository
}

// NewHelloWorldService is the provider for the hello world domain
func NewHelloWorldService(repo Repository) *Service {
	log.Println("Creating new Hello World Service...")
	return &Service{
		repo: repo,
	}
}

// HelloWorld accepts a string indicating a language and retrieves the 'hello world' message in that language if available.
func (s *Service) HelloWorld(lang string) (string, error) {
	log.Printf("[Hello World Domain] Getting %s Hello World message", lang)
	msg, err := s.repo.GetHelloWorldByLanguage(lang)
	if err != nil {
		return "", errors.Wrapf(err, "failed to get %s Hello World message", lang)
	}
	return msg, nil
}
