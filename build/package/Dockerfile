############################
# STEP 1 build executable binary
############################
# golang:1.15-alpine
FROM golang@sha256:73182a0a24a1534e31ad9cc9e3a4bb46bb030a883b26eda0a87060f679b83607 as builder

# Install git + SSL ca certificates.
# Git is required for fetching the dependencies.
# Ca-certificates is required to call HTTPS endpoints.
RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

# Create appuser
RUN adduser -D -g '' appuser

# RUN mkdir /app
WORKDIR /app

COPY go.mod go.sum ./

# Fetch dependencies.
# Using go mod.
RUN go mod download

COPY . .

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/app cmd/server/main.go

############################
# STEP 2 build a small image
############################
FROM scratch

# Import from builder.
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd

# Copy our static executable
COPY --from=builder /go/bin/app /go/bin/app

# Copy hello world languages file
COPY --from=builder /app/helloWorldLanguages.json ./

# Use an unprivileged user.
USER appuser

# Port on which the service will be exposed.
EXPOSE 8080

# Run the app binary.
ENTRYPOINT ["/go/bin/app"]
