package main

import (
	"context"
	"log"
	"os"
	"time"

	"gitlab.com/iadgarov/go-msvc/wire"
)

const (
	stopTimeout   = 15
	exitCodeError = 1
	exitCodeValid = 0
)

func main() {
	app, err := wire.InitializeApp()
	if err != nil {
		log.Println("Failed to initialize app:", err.Error())
		os.Exit(exitCodeError)
	}

	if err := app.Go(); err != nil {
		log.Println("Failed to start app:", err.Error())
		os.Exit(exitCodeError)
	}

	stopCtx, stopCancel := context.WithTimeout(context.Background(), stopTimeout*time.Second)
	if err := app.Stop(stopCtx); err != nil {
		log.Println("Failed to gracefully stop app:", err)
		stopCancel()
		os.Exit(exitCodeError)
	}
	stopCancel()

	os.Exit(exitCodeValid)
}
