module gitlab.com/iadgarov/go-msvc

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/google/wire v0.4.0
	github.com/pkg/errors v0.9.1
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	golang.org/x/text v0.3.3 // indirect
)
