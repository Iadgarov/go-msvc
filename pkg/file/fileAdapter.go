package file

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/pkg/errors"
)

const (
	filepath = "helloWorldLanguages.json"
)

// Adapter defines the file adapter
type Adapter struct {
	langs map[string]string
}

// NewFileAdapter serves as the provider for the file repository adapter
func NewFileAdapter() (*Adapter, error) {
	log.Println("Creating new file repository adapter...")
	m, err := loadFile()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create file adapter")
	}
	return &Adapter{
		langs: m,
	}, nil
}

func loadFile() (map[string]string, error) {
	log.Printf("Loading file '%s'...", filepath)
	hwLangs, err := os.Open(filepath)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to open file '%s'", filepath)
	}
	defer hwLangs.Close()
	hwBytes, err := ioutil.ReadAll(hwLangs)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to read file '%s'", filepath)
	}
	res := map[string]string{}
	if err := json.Unmarshal(hwBytes, &res); err != nil {
		return nil, errors.Wrapf(err, "failed to unmarshal file '%s' contents", filepath)
	}
	return res, nil
}

// GetHelloWorldByLanguage gets a string representing a language and returns the corresponding 'hello world' message if found in the file
func (a *Adapter) GetHelloWorldByLanguage(lang string) (string, error) {
	log.Printf("[File Driven Adapter] Getting 'hello world' message for language '%s' from file\n", lang)
	if res, ok := a.langs[strings.ToLower(lang)]; ok {
		return res, nil
	}
	return "", errors.Errorf("unsupported language '%s'", lang)
}
