#!/bin/bash
#
# Code coverage generation

COVERAGE_DIR="${COVERAGE_DIR:-coverage}"
PKG_LIST=$(go list ./... | grep -v /vendor/ | grep -v /mocks | grep -v /ingecting) ;
PKG_LIST_CSV=$(echo $PKG_LIST | tr ' ' ',')



# Create the coverage files directory
mkdir -p "$COVERAGE_DIR";

go test -covermode=count -coverprofile "${COVERAGE_DIR}/coverage.cov" -coverpkg=$PKG_LIST_CSV $PKG_LIST ;

RESULT=$(go tool cover -func="${COVERAGE_DIR}"/coverage.cov | tee /dev/stderr) ;

# If needed, generate HTML report
if [ "$1" == "html" ]; then
    go tool cover -html="${COVERAGE_DIR}"/coverage.cov -o coverage.html ;
fi


# Assert the coverage is above the desired limit
# If limit is not specified assume zero (always pass)

PERCENT_RESULT=`echo ${RESULT##*total} | awk '{ print $3 }'` 

INT_RESULT=${PERCENT_RESULT%\.*}
if [ -z "$MIN_COVERAGE" ]; then
      echo "MIN_COVERAGE not set, setting to zero."
      MIN_COVERAGE=0 ;
fi

    
if [ ${INT_RESULT} -lt $MIN_COVERAGE ]; then
    echo "Code Coverage Too Low!" ;
    echo "Should be " ${MIN_COVERAGE}% ;
    echo  "But is " ${INT_RESULT}%
    exit 1 ;
fi


# Remove the coverage files directory
rm -rf "$COVERAGE_DIR";
