//+build wireinject

package wire

import (
	"gitlab.com/iadgarov/go-msvc/app"
	"gitlab.com/iadgarov/go-msvc/app/drivers/http"
	"gitlab.com/iadgarov/go-msvc/app/helloworld"
	"gitlab.com/iadgarov/go-msvc/pkg/file"

	"github.com/google/wire"
)

// InitializeApp injects a new App with its dependencies
func InitializeApp() (*app.App, error) {
	wire.Build(wire.NewSet(
		app.NewApp,

		http.NewDriver,
		wire.Bind(new(app.Driver), new(*http.Driver)),

		helloworld.NewHelloWorldService,
		wire.Bind(new(http.HelloWorld), new(*helloworld.Service)),

		file.NewFileAdapter,
		wire.Bind(new(helloworld.Repository), new(*file.Adapter)),
	))

	return &app.App{}, nil

}
